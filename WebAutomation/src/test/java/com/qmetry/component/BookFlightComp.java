package com.qmetry.component;

import org.openqa.selenium.remote.RemoteWebElement;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class BookFlightComp extends QAFWebComponent{


	@FindBy(locator = "bookflight.flightName.text")
	private QAFWebElement flightName;
	@FindBy(locator = "bookflight.depart.rdobtn")
	private QAFWebElement departRdoBtnField;
	@FindBy(locator = "bookflight.arrive.rdobtn")
	private QAFWebElement arriveRdoBtnField;
	
	
	
	public QAFWebElement getFlightName() {
		return flightName;
	}

	public QAFWebElement getDepartRdoBtnField() {
		return departRdoBtnField;
	}

	public QAFWebElement getArriveRdoBtnField() {
		return arriveRdoBtnField;
	}



	/*
	 * @FindBy(locator="bookflight.radiobtn1.radiobtn") public QAFWebElement btn1;
	 * 
	 * 
	 * @FindBy(locator="bookflight.radiobtn2.radiobtn") public QAFWebElement btn2;
	 * 
	 * @FindBy(locator="bookflight.radiobtn3.radiobtn") public QAFWebElement btn3;
	 * 
	 * 
	 * @FindBy(locator="bookflight.radiobtn4.radiobtn") public QAFWebElement btn4;
	 * 
	 * @FindBy(locator = "bookflight.Arraivalradiobtn1.radiobtn") public
	 * QAFWebElement arrivalbtn1;
	 * 
	 * @FindBy(locator = "bookflight.Arraivalradiobtn2.radiobtn") public
	 * QAFWebElement arrivalbtn2;
	 * 
	 * @FindBy(locator = "bookflight.Arraivalradiobtn3.radiobtn") public
	 * QAFWebElement arrivalbtn3;
	 * 
	 * @FindBy(locator = "bookflight.Arraivalradiobtn4.radiobtn") public
	 * QAFWebElement arrivalbtn4;
	 * 
	 * @FindBy(locator = "bookflight.rowDetails.row") private QAFWebElement
	 * rowDetails;
	 * 
	 * @FindBy(locator = "bookflight.radio.btn") private QAFWebElement radio;
	 * 
	 * @FindBy(locator = "bookflight.flightName.text") private QAFWebElement
	 * flightName;
	 * 
	 * public QAFWebElement getflightName() { return flightName; }
	 * 
	 * public QAFWebElement getRowDetails() { return rowDetails; }
	 * 
	 * public QAFWebElement getRadio() { return radio; }
	 */
	public BookFlightComp(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	

}
