package com.qmetry.Test;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.CardNoDetailsPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20519_CardNoDetailsPageTest extends WebDriverTestCase{
     
	public CardNoDetailsPage page;
	
	@BeforeSuite
	  public void setUp() {
		page = new CardNoDetailsPage();
		page.launchPage(null);
		
	  }

	@QAFDataProvider(key="cardNo.moreThan16") 
	@Test(priority = 1, description = "field validation", enabled = true)
	public void cardNoTest1(Map<String, String> data) {
		page.setUp();
		String cardNo1 =(String) data.get("Data1");
		page.cardValidation1(cardNo1);
		page.getSearchPurchase().click();
		page.getlogoutLink();
	
	}
	
	@QAFDataProvider(key="cardNo.invalidData1") 
	@Test(priority = 2, description = "field validation")
	public void cardNoTest2(Map<String, String> data) {
		page.setUp();
		String cardNo2 =(String) data.get("Data2");
		page.cardValidation2(cardNo2);
		page.getSearchPurchase().click();
		page.getlogoutLink();
	
	} 
	
	@QAFDataProvider(key="cardNo.LessThan16") 
	@Test(priority = 3, description = "field validation")
	public void cardNoTest3(Map<String, String> data) {
		page.setUp();
		String cardNo3 =(String) data.get("Data3");
		page.cardValidation3(cardNo3);
		page.getSearchPurchase().click();
		page.getlogoutLink();
		
   		
	}

	@QAFDataProvider(key="cardNo.validData") 
	@Test(priority = 4, description = "field validation")
	public void cardNoTest4(Map<String, String> data) {
		String cardNo4 =(String) data.get("Data4");
		page.cardValidation4(cardNo4);
		page.getSearchPurchase().click();
		page.getlogoutLink();
		
	}
	
	

}
