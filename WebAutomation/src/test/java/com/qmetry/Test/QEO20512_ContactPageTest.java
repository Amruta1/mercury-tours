package com.qmetry.Test;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.ContactPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20512_ContactPageTest extends WebDriverTestCase{
	ContactPage contactPage;
	
	@BeforeSuite
	public void setUp() {
		contactPage= new ContactPage();
		contactPage.launchPage(null);
	}
	
	@Test(priority = 1,description="Verify conatct page Link")
	public void verifyContactPage() {
		contactPage.contactLink();
		Reporter.log(contactPage.getwarningmsg().getText());
		
	}
	@Test(priority=2, description = "Back to safty button")
	public void backToSaftyBtn() {
		contactPage.backtoSaftyLink();
	}
}
