package com.qmetry.Test;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.qmetry.pages.RegistrationPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20513_RegistrationPageTest extends WebDriverTestCase {
	RegistrationPage regPage;

	@BeforeSuite
	public void setUp() {
		regPage = new RegistrationPage();
		regPage.launchPage(null);
	}

	@Test(priority = 1, description = "click on Registration link")
	public void clickOnRegBtn() {
		regPage.clickOnRegister();
	}

	//@QAFDataProvider(key = "Registration.data")
	@Test(priority = 2, description = "Fetch data from bean")
	public void datadriven_bean() {
		regPage.registrationd_BeanForm();

	}

	@Test(priority = 3, description = "Click on submit button")
	public void clickonSubmit() {
		regPage.clickOnSubmitBtn();
		Reporter.log(regPage.confirmationMsg.getText()+regPage.confirmationMsg1.getText()+regPage.confirmationNote.getText());
		Reporter.log("User Sucessfully Register");

	}

	   

	 
}
