package com.qmetry.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qmetry.pages.HomePage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class QEO20508_HomePageTest extends WebDriverTestCase {
	HomePage homePage;
	@BeforeMethod
	public void verify() {
		homePage= new HomePage();
		homePage.launchPage(null);
		
	}
	@Test
	public void verifyAllLinks() {
		homePage.verifyLeftMenuLink();
		homePage.veriftTopMenuLink();
		homePage.verifyMiddleMenuLink();
	    
	    
	}
	
}
