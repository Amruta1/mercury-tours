package com.qmetry.Test;

import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.pages.RegistrationPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;

public class QEO20514_InvalidRegistrationPageTest extends WebDriverTestCase {
   @QAFDataProvider(dataFile = "resources/data/InalidDataRegistration.xls", sheetName = "Sheet1", key ="InvalidData")
   @Test(description = "Invalid Registration data from excel sheet")
   public void InvalidRegData(Map<String, String> data) {
	   RegistrationPage invalidReg = new RegistrationPage();
	   invalidReg.launchPage(null);
	   invalidReg.clickOnRegister();
	   invalidReg.invalidData(data.get("Firstname"), data.get("Lastname"), data.get("Phone"), data.get("Email"));
	   invalidReg.clickOnSubmitBtn();
	  
   }

}