package com.qmetry.Test;

import java.util.Map;

import org.testng.annotations.Test;

import com.qmetry.pages.LoginBtnPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class QEO20511_Invalid_Login extends WebDriverTestCase{
   
	
	@QAFDataProvider(key = "login.data")
	@Test( description ="Invalid data fetch from xml file" )
	public void validatelLoginTest_DD(Map<String, String> data) {
		LoginBtnPage loginbtn = new LoginBtnPage();
		loginbtn.launchPage(null);
		String username = (String) data.get("user_name");
		String password = (String) data.get("password");
		loginbtn.invalidLoginData(username,password);
    	
    
	}
}
