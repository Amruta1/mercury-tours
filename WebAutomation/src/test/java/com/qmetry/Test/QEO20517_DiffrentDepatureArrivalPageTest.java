package com.qmetry.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.DiffrentDepatureArrivalPage;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class QEO20517_DiffrentDepatureArrivalPageTest extends WebDriverTestCase{
	DiffrentDepatureArrivalPage data ;
	
	
	@BeforeSuite
	public void setUp() {
		 data = new DiffrentDepatureArrivalPage();
		 data.launchPage(null);
	}
	
	@BeforeMethod
	public void siginbtn() {
		
		data.setUp();
	}
	
	@Test(priority = 1,description = "PastDate" )
	public void diffrentData() {
		
		data.PastDate();
		data.SignOffBtn();
	}
	@Test(priority = 2, description = "futurePastDate")
	public void futurePastDate() {
		data.futurePastDate();
		data.SignOffBtn();
	}
	
	
	@Test(priority = 3)
	public void pastFutureDate() {
		data.pastFutureDate();
		data.SignOffBtn();
	}
	
	
	

}
