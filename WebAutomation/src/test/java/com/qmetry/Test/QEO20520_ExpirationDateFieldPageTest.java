package com.qmetry.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.ExpirationDateFieldPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20520_ExpirationDateFieldPageTest extends WebDriverTestCase{
	
	ExpirationDateFieldPage page;
	
	@BeforeSuite
	public void suitePage() {
		page = new ExpirationDateFieldPage();
		page.launchPage(null);
		
	}
	
	@BeforeMethod
	public void setUpTest() {
		page.setUp();
		
	}
	
	@Test(priority = 1,description = "data 1")
	public void expirationDateField1Test() {
		page.expirationDateField1();
		page.getLogoutLink().click();
		
	}
	
	@Test(priority = 2, description = "data 2")
	public void expirationDateField2Test() {
		page.expirationDateField2();
		page.getLogoutLink().click();
	}
	
	@Test(priority = 3, description = "data 3")
	public void expirationDateField3Test() {
		page.expirationDateField3();
		page.getLogoutLink().click();
	}
}