package com.qmetry.Test;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.FieldValidationPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20518_FieldValidationPageTest extends WebDriverTestCase{
      public FieldValidationPage page ;
	
	  @BeforeSuite
	  public void setUp() {
		  page = new FieldValidationPage();
		  page.launchPage(null);
		  page.setUp();
	  }
	  
	
	@QAFDataProvider(dataFile ="resources/data/fieldValidations.xls", key="FieldValidation") 
	@Test(priority = 2 , description = "field validation")
	public void fieldValidationTest(Map<String, String> data) {
		
		
		page.fieldValidation(data.get("FirstName"), data.get("LastName"));
        page.getSearchPurchase();
		page.getbackToflightbtn().click();
	}
	
}
