package com.qmetry.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


import com.qmetry.pages.BookFlightPage;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20515_BookFlightcompPageTest extends WebDriverTestCase{

	public BookFlightPage page;
	
	@BeforeSuite
	public void Setup() {
	page = new BookFlightPage();
	page.launchPage(null);
	}
	
    @BeforeMethod
	public void loginPage() {
		page.setUp();
	}
	
	@Test
	public void radioBtnDepart() {
		page.roundtrip();
		page.selectDepartFlight("Unified Airlines 563", "Blue Skies Airlines 651");
		page.getnextContinueBtn().click();
		page.depaturetitle.verifyText(page.depaturetitle.getText(), "Portland to New York ");
		Reporter.log(page.depaturetitle.getText());
		page.departFlightName.verifyText(page.departFlightName.getText(), "Unified Airlines 563");
		Reporter.log(page.departFlightName.getText());
		page.priceDepart.verifyText(page.priceDepart.getText(), "125");
		Reporter.log("Price-"+page.priceDepart.getText());
		page.arrivaltitle.verifyText(page.arrivaltitle.getText(), "New York to Portland");
		Reporter.log(page.arrivaltitle.getText());
		page.arrivalFlightName.verifyText(page.arrivalFlightName.getText(), "Blue Skies Airlines 651");
		Reporter.log(page.arrivalFlightName.getText());
		page.priceArrival.verifyText(page.priceArrival.getText(), "99");
		Reporter.log(page.priceArrival.getText());
		page.passengers.verifyText(page.passengers.getText(), "1");
		Reporter.log("Passengers-"+page.passengers.getText());
		page.taxes.verifyText(page.taxes.getText(), "$18");
		Reporter.log("taxes-"+page.taxes.getText());
		page.totalPrice.verifyText(page.totalPrice.getText(), "99");
		Reporter.log("totalPrice-"+page.totalPrice.getText());
	}
		
		
		
		
		
	}
	

