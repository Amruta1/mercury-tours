package com.qmetry.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.PopUpMsgPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class QEO20521_PopUpMsgPageTest extends WebDriverTestCase{
	
	PopUpMsgPage page;
	
	@BeforeSuite
	public void launchpage() { 
		page = new PopUpMsgPage();
		page.launchPage(null);
		
	}
	
	@BeforeMethod
	public void login() {
		page.setUp();
	}
	
	@Test
	public void popUpMsgtest() {
		page.popUpMSG();
		Reporter.logWithScreenShot("Pop msg Print");
	}
 
}
