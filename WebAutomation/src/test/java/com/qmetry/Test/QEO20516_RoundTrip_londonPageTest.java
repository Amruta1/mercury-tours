package com.qmetry.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.RoundTrip_londonPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class QEO20516_RoundTrip_londonPageTest extends WebDriverTestCase{
	RoundTrip_londonPage trip;
	
	@BeforeSuite
	public void setup() {
		trip = new RoundTrip_londonPage();
		trip.launchPage(null);
		
	}
	@BeforeMethod
	public void SetUproundtripLondon() {
		trip.setUp();
	    
	    
	}
	@Test(description = "Add data into filght page")
    public void roundtripTest() {
       trip.roundtrip();
	   
	}
	
	
}
