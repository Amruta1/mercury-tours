package com.qmetry.Test;


import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.LogOutBtnPage;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;


public class QEO20510_LogOutPageTest extends WebDriverTestCase{
	LogOutBtnPage logoutBtn;
	@BeforeSuite
	public void setUp() {
		logoutBtn= new LogOutBtnPage();
    	logoutBtn.launchPage(null);
	}
	@Test(priority=1, description ="Login to WebSite")
	public void login() {
		logoutBtn.dologin("guest","guest");
		
	}
	
    @Test(priority=2, description ="LogOut session")
    public void logOut() {
    	
    	logoutBtn.doLogOut();
    }
    
   
    
}
