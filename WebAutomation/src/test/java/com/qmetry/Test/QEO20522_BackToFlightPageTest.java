package com.qmetry.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.qmetry.pages.BackToFlightsBtnPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class QEO20522_BackToFlightPageTest extends WebDriverTestCase {
	BackToFlightsBtnPage	page;
    
	
	@BeforeSuite
	public void launch() {
		page = new BackToFlightsBtnPage();
		page.launchPage(null);
	}
	
	@BeforeMethod
	public void setUpTest() {

		page.setUp();
		
	}
	
	@Test
	public void backToFlightTest() {
		page.backToFlightBtn();
	}
}
