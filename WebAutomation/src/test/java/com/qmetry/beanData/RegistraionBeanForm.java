package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegistraionBeanForm  extends BaseFormDataBean{
	
	@Randomizer(length=15, type=RandomizerTypes.LETTERS_ONLY)
	@UiElement( fieldLoc = "registration.firstname.text")
    private String firstname;

	@Randomizer(length=10, type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registration.lastname.text")
	private String lastname;
	
	@Randomizer(length=15, type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc = "registration.phone.text")
	private String phone;
	
	@Randomizer(length=15, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.emailid.text")
	private String emailid;
	
	//contact information
	@Randomizer(length=20, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.address1.text")
	private String address1;
	
	@Randomizer(length=10, type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registration.city.text")
	private String city;
	
	@Randomizer(length=10, type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registration.state.text")
	private String state;
	
	@Randomizer(length=10, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.postalCode.text")
	private String postalCode;
	
	@Randomizer(length=10, type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registration.country.dropdown", fieldType = Type.selectbox)
	private String country;
	
	//user information
	@Randomizer(length=10, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.username.text")
	private String username;
	
	@Randomizer(length=10, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.password.text")
	private String password;
	
	@Randomizer(length=10, type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registration.confirmpwd.text")
	private String confirmpwd;

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmailid() {
		return emailid;
	}

	public String getAddress1() {
		return address1;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmpwd() {
		return confirmpwd;
	}
	
	
	
	
	
	
	
	
}
