package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;

public class PopUpMsgBean extends BaseFormDataBean{

	
	@UiElement(fieldLoc = "opUpMsg.country.dropdown", fieldType = Type.selectbox)
	private String Country;

	public String getCountry() {
		return Country;
	}
	
	
}
