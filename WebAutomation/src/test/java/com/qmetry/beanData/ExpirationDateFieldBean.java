package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;

public class ExpirationDateFieldBean extends BaseFormDataBean{
	
	@UiElement(fieldLoc = "expirationdate.cardNo.text")
	private String CardNo;
	
	@UiElement(fieldLoc = "expirationdate.month.dropdown", fieldType =Type.selectbox)
	private String month;
	
	@UiElement(fieldLoc = "expirationdate.year.dropdown", fieldType =Type.selectbox)
	private String year;


	public String getCardNo() {
		return CardNo;
	}

	public String getMonth() {
		return month;
	}

	public String getYear() {
		return year;
	}
}
