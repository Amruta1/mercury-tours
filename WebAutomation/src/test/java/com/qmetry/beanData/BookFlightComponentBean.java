package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;

public class BookFlightComponentBean extends BaseFormDataBean{

	@UiElement(fieldLoc = "bookflightcomponent.departingfrom.dropdown" , fieldType = Type.selectbox)
	private String departingfrom;
	
	@UiElement(fieldLoc = "bookflightcomponent.fromMonth.dropdown",fieldType = Type.selectbox)
    private String fromMonth;
	
	@UiElement(fieldLoc = "bookflightcomponent.fromDay.dropdown",fieldType = Type.selectbox)
	private String  fromDay;
	
	@UiElement(fieldLoc = "bookflightcomponent.arrivingIn.dropdown" ,fieldType = Type.selectbox)
    private String arrivingIn;
	
	@UiElement(fieldLoc = "bookflightcomponent.returningtoMonth.dropdown",fieldType = Type.selectbox)
	private String returningtoMonth;
	
	@UiElement(fieldLoc = "bookflightcomponent.ReturningtoDay.dropdown", fieldType = Type.selectbox)
	private String ReturningtoDay;
	
	
	
	@UiElement(fieldLoc = "bookflightcomponent.airline.dropdown",fieldType = Type.selectbox)
	private String airline;
	 
	

	

	

	public String getDepartingfrom() {
		return departingfrom;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public String getFromDay() {
		return fromDay;
	}

	public String getArrivingIn() {
		return arrivingIn;
	}

	public String getReturningtoMonth() {
		return returningtoMonth;
	}

	public String getReturningtoDay() {
		return ReturningtoDay;
	}

	

	public String getAirline() {
		return airline;
	}

}
