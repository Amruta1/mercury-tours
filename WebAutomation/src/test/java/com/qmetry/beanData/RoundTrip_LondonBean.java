package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;

public class RoundTrip_LondonBean extends BaseFormDataBean{
	
	
	
	@UiElement(fieldLoc = "roundTripLondon.departingfrom.dropdown" , fieldType = Type.selectbox)
	private String departingfrom;
	
	@UiElement(fieldLoc = "roundTripLondon.fromMonth.dropdown",fieldType = Type.selectbox)
    private String fromMonth;
	
	@UiElement(fieldLoc = "roundTripLondon.fromDay.dropdown",fieldType = Type.selectbox)
	private String  fromDay;
	
	@UiElement(fieldLoc = "roundTripLondon.arrivingIn.dropdown" ,fieldType = Type.selectbox)
    private String arrivingIn;
	
	@UiElement(fieldLoc = "roundTripLondon.returningtoMonth.dropdown",fieldType = Type.selectbox)
	private String returningtoMonth;
	
	@UiElement(fieldLoc = "roundTripLondon.ReturningtoDay.dropdown", fieldType = Type.selectbox)
	private String ReturningtoDay;
	
	
	
	@UiElement(fieldLoc = "roundTripLondon.airline.dropdown",fieldType = Type.selectbox)
	private String airline;
	 
	

	

	

	public String getDepartingfrom() {
		return departingfrom;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public String getFromDay() {
		return fromDay;
	}

	public String getArrivingIn() {
		return arrivingIn;
	}

	public String getReturningtoMonth() {
		return returningtoMonth;
	}

	public String getReturningtoDay() {
		return ReturningtoDay;
	}

	

	public String getAirline() {
		return airline;
	}

	
	


}
