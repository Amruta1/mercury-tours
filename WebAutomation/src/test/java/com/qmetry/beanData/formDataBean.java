package com.qmetry.beanData;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.util.Randomizer;


public class formDataBean extends BaseFormDataBean{

	
	@Randomizer(length=15)
	@UiElement(fieldLoc="register.firstname.txt",order=1)
	private String firstName;
	@Randomizer(length=15)
	@UiElement(fieldLoc="register.lastname.txt",order=2)
	private String lastName;
	@Randomizer(length=15)
	@UiElement(fieldLoc="register.state.txt",order=7)
	private String state;
	@Randomizer(length=10)
	@UiElement(fieldLoc="register.city.txt",order=6)
	private String city;
	@Randomizer(length=10)
	@UiElement(fieldLoc="register.phone.txt",order=3)
	private String phoneNo;
	@Randomizer(length=10,suffix="@gmail.com")
	@UiElement(fieldLoc="register.email.txt",order=4)
	private String email;
	@Randomizer(length=30)
	@UiElement(fieldLoc="register.address.txt",order=5)
	private String address;
	@Randomizer(length=6)
	@UiElement(fieldLoc="register.postalcode.txt",order=8)
	private String postcode;
	@Randomizer(length=10)
	@UiElement(fieldLoc="register.country.dropdown",order=9)
	private String country;
	
	@Randomizer(length=10)
	@UiElement(fieldLoc="register.username.txt",order=10)
	private String username;
	@Randomizer(length=8)
	@UiElement(fieldLoc="register.password.txt",order=11)
	private String password;
	@Randomizer(length=8)
	@UiElement(fieldLoc="register.confirmPassword.txt",order=12)
	private String confirmpassword;
	
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public String getState() {
		return state;
	}
	public String getCity() {
		return city;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public String getEmail() {
		return email;
	}
	public String getAddress() {
		return address;
	}
	public String getPostcode() {
		return postcode;
	}
	public String getCountry() {
		return country;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getConfirmPassword() {
		return confirmpassword;
	}


}
