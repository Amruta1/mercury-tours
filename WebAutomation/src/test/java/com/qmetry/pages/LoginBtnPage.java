package com.qmetry.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LoginBtnPage extends WebDriverBaseTestPage<WebDriverTestPage>{
    
	@FindBy(locator="login.username.text")
    QAFWebElement usernameField;

    @FindBy(locator="login.password.text")
    QAFWebElement passwordField;

    @FindBy(locator="login.login.btn")
    QAFWebElement loginBtn;
	
    public QAFWebElement getUserNameField() {
    	return usernameField;
    	
    }
    
    public QAFWebElement getpasswordField() {
    	return passwordField;
    }
    
    public QAFWebElement getloginBtn() {
    	return loginBtn;
    }
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	 @QAFTestStep(description ="Login into website")
	    public void dologin(String username, String password) {
		    String loginPageTitle = driver.getTitle();
			String expLoginPageTitle ="Welcome: Mercury Tours";
			Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
			
	    	usernameField.waitForPresent(1000);
	    	getUserNameField().sendKeys(username);
	    	passwordField.waitForPresent(2000);
	    	getpasswordField().sendKeys(password);
	    	getloginBtn().click();
	        
	    	String actHomepagetitle = driver.getTitle();
			String expHomePageTitle ="Find a Flight: Mercury Tours";
			Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on HomePage", "User on HomePage");
			
	    	
	    	Reporter.log("Login Sucessfully");
	    }
      
	//data driven approach fetch data from xml data source
	    @QAFTestStep(description = "Invalid Login Data from xml")
	    public void invalidLoginData(String username1, String password1) {
	    	
	    	
	    	getUserNameField().sendKeys(username1);
	    	getpasswordField().sendKeys(password1);
	    	getloginBtn().click();
	    	
	    	String loginPageTitle = driver.getTitle();
			String expLoginPageTitle ="Sign-on: Mercury Tours";
			Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on Sign-on page", "User on Sign-on page");
				
	    	
	    	
		}

}
