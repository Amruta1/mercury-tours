package com.qmetry.pages;


import org.testng.Reporter;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class ContactPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="contactpage.contact.link")
	QAFWebElement contactLink;
	
	@FindBy(locator="contactpage.backtosafty.link")
	QAFWebElement backtosaftyLink;
	
	@FindBy(locator="contactpage.warningmsg.text")
	QAFWebElement warningmsg;
	
	public QAFWebElement getContactPageLink() {
		return contactLink;
	}
	 
	public QAFWebElement getBacktosaftyLink() {
		return backtosaftyLink;
	}
	
	public QAFWebElement getwarningmsg() {
		return warningmsg;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	
	 @QAFTestStep(description="verify contact page")
	  public void contactLink() {
		 String loginPageTitle = driver.getTitle();
		 String expLoginPageTitle ="Welcome: Mercury Tours";
		 Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
		
		 getContactPageLink().click();
		 String contactPageTitle = driver.getTitle();
		 String expcontactPageTitle ="Under Construction: Mercury Tours";
		 Validator.assertTrue(contactPageTitle.contains(expcontactPageTitle), "User not on contact", "User on contact");
		 
		
	}
	 
	 @QAFTestStep(description ="go to back to safty button")
	 public void backtoSaftyLink() {
		 getBacktosaftyLink().click();
		 String loginPageTitle = driver.getTitle();
		 String expLoginPageTitle ="Welcome: Mercury Tours";
		 Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
			
	 }

}
