package com.qmetry.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LogOutBtnPage extends WebDriverBaseTestPage<WebDriverTestPage> {
    @FindBy(locator="logoutbtn.username.text")
	    QAFWebElement usernameField;
    
    @FindBy(locator="logoutbtn.password.text")
    QAFWebElement passwordField;
    
    @FindBy(locator="logoutbtn.login.btn")
    QAFWebElement loginBtn;
    
    @FindBy(locator="logoutbtn.logout.link")
    QAFWebElement logoutLink;
    
    public QAFWebElement getUserNameField() {
    	return usernameField;
    	
    }
    
    public QAFWebElement getpasswordField() {
    	return passwordField;
    }
    
    public QAFWebElement getloginBtn() {
    	return loginBtn;
    }

    public QAFWebElement getLogOutBtn() {
	return logoutLink;
}

    @Override
    protected void openPage(PageLocator locator, Object... args) {
	driver.get("/");
	
}


    @QAFTestStep(description ="Login into website")
    public void dologin(String username, String password) {
    	String loginPageTitle = driver.getTitle();
		String expLoginPageTitle ="Welcome: Mercury Tours";
		Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
		
    	usernameField.waitForPresent(1000);
    	getUserNameField().sendKeys(username);
    	passwordField.waitForPresent(2000);
    	getpasswordField().sendKeys(password);
    	getloginBtn().click();
        
    	String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on Find a Flight", "User on Find a Flight");
		
    	
    }

    @QAFTestStep(description= "Check LogOut Btn")
    public void doLogOut() {
	    logoutLink.click();
	    String loginPageTitle = driver.getTitle();
		String expLoginPageTitle ="Sign-on: Mercury Tours";
		Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on Sign-on", "User on Sign-on");
		
}
    
}
