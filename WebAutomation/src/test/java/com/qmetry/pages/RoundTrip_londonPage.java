package com.qmetry.pages;

import org.testng.Reporter;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.beanData.RoundTrip_LondonBean;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class RoundTrip_londonPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator="roundTripLondon.continuebtn.btn")
	private QAFWebElement continuebtn;
	
	
	@FindBy(locator="roundTripLondon.londontolondon.text")
	private QAFWebElement londonToLondon;
	
	public QAFWebElement getContinueBtn() {
		return continuebtn;
	}
	
	public QAFWebElement getlondonToLondon() {
		return londonToLondon;
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	
    @QAFTestStep(description = "Login to page")
	public void setUp() {
    	QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
    	login.login();
    	String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
	}
   
    
	@QAFTestStep(description="fetch data from BaseFromDataBean")
	public void roundtrip() {
		
		RoundTrip_LondonBean roundtrip= new RoundTrip_LondonBean();
		roundtrip.fillFromConfig("flight.book.data");
		roundtrip.fillUiElements();
		continuebtn.click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		Reporter.log(getlondonToLondon().getText());
	}
	
	
}
