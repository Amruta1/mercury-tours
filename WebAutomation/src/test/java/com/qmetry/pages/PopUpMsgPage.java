package com.qmetry.pages;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;

import com.qmetry.Test.QEO20509_LoginPageTest;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class PopUpMsgPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "popUpMsg.continueBtn.btn")
	private QAFWebElement continueBtn;
	
	@FindBy(locator = "popUpMsg.nextContinueBtn.btn")
	private QAFWebElement nextContinueBtn;
	
	
	@FindBy(locator = "popUpMsg.country.dropdown")
    public QAFWebElement country;	
	
	public QAFWebElement getContinueBtn() {
		return continueBtn;
	}

	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	
    @QAFTestStep(description = "Login into page")
	public void setUp() {
       QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
	   login.login();
	   String actHomepagetitle = driver.getTitle();
	   String expHomePageTitle ="Find a Flight: Mercury Tours";
	   Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
	   getContinueBtn().click();
	   String actSelectFlightpagetitle = driver.getTitle();
	   String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
	   Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		getNextContinueBtn().click();
		String actBookaFlightPageTitle= driver.getTitle();
		String expBookaFlightPageTitle ="Book a Flight: Mercury Tours";
		Validator.assertTrue(actBookaFlightPageTitle.contains(expBookaFlightPageTitle), "User not on Flight a flight Page", "User on  Flight a flight Page");
		
		}
	
	
	@QAFTestStep(description = "Pop Up msg")
    public void popUpMSG() {

		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        Reporter.log("Page Scroll Down till the End :");
       
		Select select = new Select(country);
		select.selectByIndex(3);

	}
}
