package com.qmetry.pages;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FieldValidationPage  extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="fieldValidation.continuebtn.btn")
	QAFWebElement continueBtn;
	
	@FindBy(locator = "fieldValidation.nextcontinuebtn.btn")
	QAFWebElement nextContinueBtn;
	
	@FindBy(locator = "fieldValidation.firstname.text")
	QAFWebElement firstName;
	
	@FindBy(locator = "fieldValidation.lastnmae.text")
	QAFWebElement lastName;
	 
	@FindBy(locator = "fieldValidation.searchpurchase.btn")
	QAFWebElement searchPurchase;
	
	@FindBy(locator = "fieldValidation.confirmationPage.lable")
	QAFWebElement confirmationPageTitle;
	
	@FindBy(locator = "fieldValidation.backToflight.lable")
	QAFWebElement backToflightbtn;
	
	public QAFWebElement getContinueBtn() {
		return continueBtn;
	}

	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}

	public QAFWebElement getFirstName() {
		return firstName;
	}

	public QAFWebElement getLastName() {
		return lastName;
	}

	public QAFWebElement getSearchPurchase() {
		return searchPurchase;
	}
	
	public QAFWebElement getConfirmationpageTitle() {
		return confirmationPageTitle;
	}

	public QAFWebElement getbackToflightbtn() {
		return backToflightbtn;
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	@QAFTestStep(description = "Login into page")
	public void setUp() {
		QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
		login.login();
		String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
	}
	
	@QAFTestStep(description = "Field validation")
    public void fieldValidation(String firstname, String lastname) {
		getContinueBtn().click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		getNextContinueBtn().click();
		String actBookaFlightPageTitle= driver.getTitle();
		String expBookaFlightPageTitle ="Book a Flight: Mercury Tours";
		Validator.assertTrue(actBookaFlightPageTitle.contains(expBookaFlightPageTitle), "User not on Flight a flight Page", "User on  Flight a flight Page");
		
		
		getFirstName().sendKeys(firstname);
		getLastName().sendKeys(lastname);
		getSearchPurchase().click();
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
		
	}
	

}
