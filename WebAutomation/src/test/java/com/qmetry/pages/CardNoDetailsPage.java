package com.qmetry.pages;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class CardNoDetailsPage  extends WebDriverBaseTestPage<WebDriverTestPage>{
             
	@FindBy(locator="cardDetails.continuebtn.btn")
	QAFWebElement continueBtn;
	
	@FindBy(locator = "bookflightcomponent.reserveflight.btn")
	QAFWebElement nextContinueBtn;
	 
	@FindBy(locator = "cardDetails.cardNo.text")
	QAFWebElement cardNoDetails;
	
	@FindBy(locator = "cardDetails.searchpurchase.btn")
	QAFWebElement searchPurchase;
	
	@FindBy(locator = "cardDetails.confirmationPage.lable")
	QAFWebElement confirmationPageTitle;
	
	@FindBy(locator = "cardDetails.backToflight.lable")
	QAFWebElement backToflightbtn;
	
	@FindBy(locator="logoutbtn.logout.link")
	private QAFWebElement logoutLink;
	
	public QAFWebElement getlogoutLink() {
		return logoutLink;
	}
	
	public QAFWebElement getContinueBtn() {
		return continueBtn;
	}

	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}

	public QAFWebElement getSearchPurchase() {
		return searchPurchase;
	}
	
	public QAFWebElement getConfirmationpageTitle() {
		return confirmationPageTitle;
	}

	public QAFWebElement getbackToflightbtn() {
		return backToflightbtn;
	}
	
	public QAFWebElement getcardNoDetails() {
		return cardNoDetails;
	}
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	@QAFTestStep(description = "Login into page")
	public void setUp() {
		QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
		login.login();
		String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
		getContinueBtn().click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		getNextContinueBtn().click();
		String actBookaFlightPageTitle= driver.getTitle();
		String expBookaFlightPageTitle ="Book a Flight: Mercury Tours";
		Validator.assertTrue(actBookaFlightPageTitle.contains(expBookaFlightPageTitle), "User not on Flight a flight Page", "User on  Flight a flight Page");
		
	}
	
	
	@QAFTestStep(description = " Card Field validation1")
    public void cardValidation1(String cardNo1) {
		
		getcardNoDetails().sendKeys(cardNo1);
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
		
	}

	@QAFTestStep(description = " Card Field validation1")
    public void cardValidation2(String cardNo2) {
		
		getcardNoDetails().sendKeys(cardNo2);
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
		
	}
	
	@QAFTestStep(description = " Card Field validation1")
    public void cardValidation3(String cardNo3) {
		
		getcardNoDetails().sendKeys(cardNo3);
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
		
	}
	
	@QAFTestStep(description = " Card Field validation1")
    public void cardValidation4(String cardNo4) {
		
		getcardNoDetails().sendKeys(cardNo4);
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
		
	}

}
