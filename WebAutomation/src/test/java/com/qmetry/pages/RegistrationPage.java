package com.qmetry.pages;

import com.qmetry.beanData.RegistraionBeanForm;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RegistrationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "registration.register.link")
	QAFWebElement registerLink;

	
	  // Contact Information
	  
	  @FindBy(locator = "registration.firstname.text") 
	  QAFWebElement firstname;
	  
	  @FindBy(locator = "registration.lastname.text") 
	  QAFWebElement lastname;
	  
	  @FindBy(locator = "registration.phone.text")
	  QAFWebElement phone;
	  
	  @FindBy(locator = "registration.emailid.text")
	  QAFWebElement emailid;
	  
	  @FindBy(locator = "registration.confirmationMsg.text")
	  public QAFWebElement confirmationMsg;
	  
	  @FindBy(locator = "registration.confirmationMsg1.text")
	  public QAFWebElement confirmationMsg1;
	  
	  @FindBy(locator = "registration.confirmationNote.text")
	  public QAFWebElement confirmationNote;
	  
	

	// submit
	@FindBy(locator = "registration.submit.btn")
	QAFWebElement submit;

	public QAFWebElement getRegisterLink() {
		return registerLink;
	}

	
	 public QAFWebElement getFirstnameText() {
		  return firstname; }
	  
	 public QAFWebElement getLastnameText() {
		  return lastname; }
	  
	 public QAFWebElement getPhoneText() { 
		  return phone; }
	  
	 public QAFWebElement getEmailidText() { 
		  return emailid; }
	
	public QAFWebElement getSubmitBtn() {
		return submit;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");

	}

	@QAFTestStep(description = "Click on registration btn")
	public void clickOnRegister() {
		String loginPageTitle = driver.getTitle();
		String expLoginPageTitle ="Welcome: Mercury Tours";
	    Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
			
		getRegisterLink().click();
		String registerPageTitle = driver.getTitle();
		String expRegisterPageTitle ="Register: Mercury Tours";
	    Validator.assertTrue(registerPageTitle.contains(expRegisterPageTitle), "User not on loginPage", "User on LoginPAge");
			
	}

	// Registration using DataBeanForm
	@QAFTestStep(description = "Registration using DataBeanForm")
	public void registrationd_BeanForm() {
		RegistraionBeanForm bean = new RegistraionBeanForm();

		bean.fillFromConfig("Registration.data");
		bean.fillUiElements();
		 
		
		
	}

	@QAFTestStep(description = "click on submit button")
	public void clickOnSubmitBtn() {
		getSubmitBtn().click();
		String registerPageTitle = driver.getTitle();
		String expRegisterPageTitle ="Register: Mercury Tours";
	    Validator.assertTrue(registerPageTitle.contains(expRegisterPageTitle), "User not on loginPage", "User on LoginPAge");
		Reporter.log(confirmationMsg.getText()+confirmationMsg1.getText()+confirmationNote.getText());	
	}

	@QAFTestStep(description = "Fetch Invalid Data from Excel Sheet")
	public void invalidData(String firstname,String lastname,String phone, String emailid  ) {
		getFirstnameText().sendKeys(firstname);
		getLastnameText().sendKeys(lastname);
		getPhoneText().sendKeys(phone);
		getEmailidText().sendKeys(emailid);
		
		
        
	}

}
