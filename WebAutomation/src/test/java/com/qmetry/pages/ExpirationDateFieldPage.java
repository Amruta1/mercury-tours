package com.qmetry.pages;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.beanData.ExpirationDateFieldBean;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ExpirationDateFieldPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "expirationdate.continuebtn.btn")
	private QAFWebElement continuebtn;
	
	@FindBy(locator = "expirationdate.nextcontinuebtn.btn")
	private QAFWebElement nextContinueBtn;
	
	@FindBy(locator="expirationdate.cardNo.text")
	private QAFWebElement cardNo;
	
	@FindBy(locator="expirationdate.searchpurchase.btn")
	private QAFWebElement searchpurchaseBtn; 
	
	@FindBy(locator="logoutbtn.logout.link")
	private QAFWebElement logoutLink;
	
	@FindBy(locator = "expirationdate.confirmationPage.lable")
	private QAFWebElement confirmationPage;
	
	@FindBy(locator = "expirationdate.month.dropdown")
	private QAFWebElement month;
	
	@FindBy(locator = "expirationdate.year.dropdown")
	private QAFWebElement year;
	
	public QAFWebElement getmonth() {
		return month;
	}
	
	public QAFWebElement getyear() {
		return year;
	}
	public QAFWebElement getConfirmationPage() {
		return confirmationPage;
	}


	public QAFWebElement getContinuebtn() {
		return continuebtn;
	}


	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}


	public QAFWebElement getCardNo() {
		return cardNo;
	}


	public QAFWebElement getSearchpurchaseBtn() {
		return searchpurchaseBtn;
	}


	public QAFWebElement getLogoutLink() {
		return logoutLink;
	}


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	
	@QAFTestStep(description = "Login into page")
	public void setUp() {
		QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
		login.login();
		String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
		getContinuebtn().click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		getNextContinueBtn().click();
		String actBookaFlightPageTitle= driver.getTitle();
		String expBookaFlightPageTitle ="Book a Flight: Mercury Tours";
		Validator.assertTrue(actBookaFlightPageTitle.contains(expBookaFlightPageTitle), "User not on Flight a flight Page", "User on  Flight a flight Page");
		
	}
	
	
    public void expirationDateField1() {
		ExpirationDateFieldBean date = new ExpirationDateFieldBean();
		date.fillFromConfig("expiration.date1");
		date.fillUiElements();
		getSearchpurchaseBtn().click();
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
	
		
	}
	
	 public void expirationDateField2() {
			ExpirationDateFieldBean date = new ExpirationDateFieldBean();
			date.fillFromConfig("expiration.date2");
			date.fillUiElements();
			getSearchpurchaseBtn().click();
			String actFlightConfirmationPageTitle= driver.getTitle();
			String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
			Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
			
		}
	 
	 public void expirationDateField3() {
			ExpirationDateFieldBean date = new ExpirationDateFieldBean();
			date.fillFromConfig("expiration.date3");
			date.fillUiElements();
			getSearchpurchaseBtn().click();
			String actFlightConfirmationPageTitle= driver.getTitle();
			String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
			Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
			
			
		}


}
