package com.qmetry.pages;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.beanData.RoundTrip_LondonBean;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class DiffrentDepatureArrivalPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="roundTripLondon.continuebtn.btn")
	QAFWebElement continuebtn;
	
	@FindBy(locator="logoutbtn.logout.link")
	QAFWebElement logoutbtn;
	
	//After SignOff Login
	@FindBy(locator="login.username.text")
	QAFWebElement username;
	
	@FindBy(locator="login.password.text")
	QAFWebElement paasword;
	
	@FindBy(locator="login.password.text")
	QAFWebElement loginbtn;
	
	public QAFWebElement getusername() {
		return username;
	}
	
	public QAFWebElement getpaasword() {
		return paasword;
	}
	
	public QAFWebElement getloginbtn() {
		return loginbtn;
	}
	
	public QAFWebElement getContinueBtn() {
		return continuebtn;
	}
	
	public QAFWebElement getLogOutbtn() {
		return logoutbtn;
	}
	

    @QAFTestStep(description = "Login to page")
	public void setUp() {
    	QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
    	login.login();
    	String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
	}
	
    @QAFTestStep(description="fetch data from BaseFromDataBean")
	public void PastDate() {
		
		RoundTrip_LondonBean roundtrip= new RoundTrip_LondonBean();
		roundtrip.fillFromConfig("flight.book.different.departure.arrival.pastDate");
		roundtrip.fillUiElements();
		getContinueBtn().click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
	}
    
    @QAFTestStep(description="fetch data from BaseFromDataBean")
   	public void futurePastDate() {
   		
   		RoundTrip_LondonBean roundtrip= new RoundTrip_LondonBean();
   		roundtrip.fillFromConfig("flight.book.different.departure.arrival");
   		roundtrip.fillUiElements();
   		getContinueBtn().click();
   		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
   	}
    
    @QAFTestStep(description="fetch data from BaseFromDataBean")
   	public void pastFutureDate() {
   		
   		RoundTrip_LondonBean roundtrip= new RoundTrip_LondonBean();
   		roundtrip.fillFromConfig("flight.book.different.departure.arrival.pastfutureDate");
   		roundtrip.fillUiElements();
   		getContinueBtn().click();
   		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
   	}
    
	@QAFTestStep(description = "Sign Off")
	public void SignOffBtn() {
		getLogOutbtn().click();
	}
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}

}
