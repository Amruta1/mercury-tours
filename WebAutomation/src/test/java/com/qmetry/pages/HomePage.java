package com.qmetry.pages;

import org.testng.Reporter;

import com.google.common.base.Verify;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	//LestMenu
	@FindBy(locator= "leftMenu.home.link")
	 private QAFWebElement homeLink;
	
	@FindBy(locator= "leftMenu.flights.link")
	 private QAFWebElement flightLink;
	
	@FindBy(locator= "leftMenu.hotels.link")
	 private QAFWebElement hotelsLink;
	
	@FindBy(locator= "leftMenu.carRentals.link")
	 private QAFWebElement carRentalsLink;
	
	@FindBy(locator= "leftMenu.cruises.link")
	 private QAFWebElement cruisesLink;
	
	@FindBy(locator= "leftMenu.destination.link")
	 private QAFWebElement destinationLink;
	
	@FindBy(locator= "leftMenu.vacation.link")
	 private QAFWebElement vacationLink;
	
	//TopMenu
	@FindBy(locator= "topMenu.signOff.link")
	 private QAFWebElement signOffLink;
	
	@FindBy(locator= "topMenu.register.link")
	 private QAFWebElement registerLink;
	
	@FindBy(locator= "topMenu.support.link")
	 private QAFWebElement supportLink;
	
	@FindBy(locator= "topMenu.contact.link")
	 private QAFWebElement contactLink;
	
	
    //MiddleMenu
	@FindBy(locator= "middleMenu.featured_Destination.link")
	 private QAFWebElement featured_DestinationLink;
	
	@FindBy(locator= "middleMenu.find_a_Flight.link")
	 private QAFWebElement find_a_FlightLink;
	
	@FindBy(locator= "middleMenu.destinationM.link")
	 private QAFWebElement destinationMLink;
	
	@FindBy(locator= "middleMenu.vacationM.link")
	 private QAFWebElement vacationMLink;
	
	@FindBy(locator= "middleMenu.registerM.link")
	 private QAFWebElement registerMLink;
	
	@FindBy(locator= "middleMenu.specials.link")
	 private QAFWebElement specialsLink;

	@FindBy(locator= "middleMenu.tour_tips.link")
	 private QAFWebElement tour_tipsLink;
	
	@FindBy(locator= "middleMenu.links.link")
	 private QAFWebElement linksLink;
	
	
	public QAFWebElement getHomeLink() {
		return homeLink;
	}
	
	public QAFWebElement getFlightLink() {
		return flightLink;
	}
	
	public QAFWebElement getHotelsLink() {
		return hotelsLink;
	}
	
	public QAFWebElement getCarRentalsLink() {
		return carRentalsLink;
	}
	
	public QAFWebElement getCruisesLink() {
		return cruisesLink;
	}
	
	public QAFWebElement getDestinationLink() {
		return destinationLink;
	}
	
	public QAFWebElement getVacationLink() {
		return vacationLink;
	}
	
	//TopMenu
	public QAFWebElement getSignOffLink() {
		return signOffLink;
	}
	
	public QAFWebElement getRegisterLink() {
		return registerLink;
	}
	
	public QAFWebElement getSupportLink() {
		return supportLink;
	}
	
	public QAFWebElement getContactLink() {
		return contactLink;
	}
	
	//MiddleMenu
	public QAFWebElement getFeatured_DestinationLink() {
		return featured_DestinationLink;
	}
	
	public QAFWebElement getFind_a_FlightLink() {
		return find_a_FlightLink;
	}
	
	public QAFWebElement getDestinationMLink() {
		return destinationMLink;
	}
	
	public QAFWebElement getVacationMLink() {
		return vacationMLink;
	}
	
	public QAFWebElement getRegisterMLink() {
		return registerMLink;
	}
	
	public QAFWebElement getSpecialsLink() {
		return specialsLink;
	}
	
	public QAFWebElement getTour_tipsLink() {
		return tour_tipsLink;
	}
	
	public QAFWebElement getLinksLink() {
		return linksLink;
	}
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	@QAFTestStep(description ="verify LeftMenu links on HomePage")
	public void verifyLeftMenuLink() {
		String loginPageTitle = driver.getTitle();
		String expLoginPageTitle ="Welcome: Mercury Tours";
		Validator.assertTrue(loginPageTitle.contains(expLoginPageTitle), "User not on loginPage", "User on LoginPAge");
		
		getHomeLink().assertPresent("Home link verify");
	    String actHome=getHomeLink().getText();
	    String expHome="Home";
	    Validator.assertTrue(actHome.contains(expHome), "Home not Present", "Home Present");
		
		getFlightLink().assertPresent("Flight link verify");
		String actFlight=getFlightLink().getText();
	    String expflight="Flight";
	    Validator.assertTrue(actFlight.contains(expflight), "Flight not Present", "Flight Present");
		
		
		getHotelsLink().assertPresent("Hotel Link verify");
		String actHotels=getHotelsLink().getText();
	    String expHotels="Hotels";
	    Validator.assertTrue(actHotels.contains(expHotels), "Hotels not Present", "Hotels Present");
	
		getCarRentalsLink().assertPresent("Car rentals link verify");
		String actCarRentals=getCarRentalsLink().getText();
	    String expCarRentals="Car Rentals";
	    Validator.assertTrue(actCarRentals.contains(expCarRentals), "Car Rentals not Present", "Car Rentals Present");
		
		getCruisesLink().assertPresent("Cruises link verify");
		String actCruises=getCruisesLink().getText();
	    String expCruises="Cruises";
	    Validator.assertTrue(actCruises.contains(expCruises), "Cruises not Present", "Cruises Present");
	
		getDestinationLink().assertPresent("destination link verify");
		String actDestination=getDestinationLink().getText();
	    String expDestination="Destination";
	    Validator.assertTrue(actDestination.contains(expDestination), "Destination not Present", "Destination Present");
	
		getVacationLink().assertPresent("vaccation Link verify");
		String actVacation=getVacationLink().getText();
	    String expVacation="Vacation";
	    Validator.assertTrue(actVacation.contains(expVacation), "Vacation not Present", "Vacation Present");
	
	}
	
	@QAFTestStep(description ="verify TopMenu links on HomePage")
	public  void veriftTopMenuLink() {
		getSignOffLink().assertPresent("Sign OFF link verify");
		String actSignOn=getSignOffLink().getText();
		System.out.println("actSignOn");
	    String expSignOn="SIGN-OFF";
	    Validator.assertTrue(actSignOn.contains(expSignOn), "SignOff not Present", "SignOff Present");
	
	    getRegisterLink().assertPresent("register link verify");
	    String actRegister=getRegisterLink().getText();
	    String expRegister="REGISTER";
	    Validator.assertTrue(actRegister.contains(expRegister), "Register not Present", "Register Present");
	
	    
	    getSupportLink().assertPresent("Support link verify");
	    String actSupport=getSupportLink().getText();
	    String expSupport="SUPPORT";
	    Validator.assertTrue(actSupport.contains(expSupport), "Support not Present", "Support Present");
	 
	    getContactLink().assertPresent("Contact Link verify");
	    String actContact=getContactLink().getText();
	    String expContact="CONTACT";
	    Validator.assertTrue(actContact.contains(expContact), "Contact not Present", "Contact Present");
	
	}
	@QAFTestStep(description ="verify MiddleMenu links")
	public void verifyMiddleMenuLink() {
	    getFeatured_DestinationLink().assertPresent("Featured destination link verify");
	    getFind_a_FlightLink().assertPresent("Find a Flight link is verify");
	    getDestinationMLink().assertPresent("DestinationM link verify");
	    getVacationMLink().assertPresent("vaccationM link verify");
	    getRegisterMLink().assertPresent("registerM link verify");
	    getLinksLink().assertPresent("Links link verify");
	    getSpecialsLink().assertPresent("Specials link verify");
	    getTour_tipsLink().assertPresent("Tour tips link verify");
	    
	}
}
