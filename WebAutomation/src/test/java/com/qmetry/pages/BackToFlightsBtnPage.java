package com.qmetry.pages;

import org.openqa.selenium.JavascriptExecutor;


import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class BackToFlightsBtnPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator = "backToFlightBtn.backToflight.lable")
	public QAFWebElement backToflight;
	
	@FindBy(locator="fieldValidation.continuebtn.btn")
	QAFWebElement continueBtn;
	
	@FindBy(locator = "fieldValidation.nextcontinuebtn.btn")
	QAFWebElement nextContinueBtn;
	
	@FindBy(locator = "cardDetails.searchpurchase.btn")
	QAFWebElement searchPurchase;
	 
	public QAFWebElement getbackToflightBtn() {
		return backToflight;
	}
	

	public QAFWebElement getContinueBtn() {
		return continueBtn;
	}

	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}

	public QAFWebElement getSearchPurchase() {
		return searchPurchase;
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	@QAFTestStep(description = "Login into page")
	public void setUp() {
		QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
		login.login();
		String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
		getContinueBtn().click();
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
		getNextContinueBtn().click();
		String actBookaFlightPageTitle= driver.getTitle();
		String expBookaFlightPageTitle ="Book a Flight: Mercury Tours";
		Validator.assertTrue(actBookaFlightPageTitle.contains(expBookaFlightPageTitle), "User not on Flight a flight Page", "User on  Flight a flight Page");
		
		getSearchPurchase().click();
		String actFlightConfirmationPageTitle= driver.getTitle();
		String expFlightConfimationPageTitle ="Flight Confirmation: Mercury Tours";
		Validator.assertTrue(actFlightConfirmationPageTitle.contains(expFlightConfimationPageTitle), "User not on Flight Confirmation Page", "User on Flight Confirmation Page");
	}
	
	
	
	

	@QAFTestStep(description = "Back to flight btn validate")
	public void backToFlightBtn() {
		
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        Reporter.log("Page Scroll Down till the End :");
        getbackToflightBtn().click();
        String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		
		
	

		
	}

}
