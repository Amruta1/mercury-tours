package com.qmetry.pages;



import java.util.List;

import com.qmetry.Test.QEO20509_LoginPageTest;
import com.qmetry.beanData.BookFlightComponentBean;

import com.qmetry.component.BookFlightComp;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import com.qmetry.qaf.automation.util.Validator;

public class BookFlightPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "bookflightcomponent.continuebtn.btn")
	QAFWebElement continuebtn;

	@FindBy(locator = "bookflightcomponent.reserveflight.btn")
	QAFWebElement nextContinueBtn;

	@FindBy(locator = "bookflight.radio.btn")
	public BookFlightComp bookflightcomponent;

	@FindBy(locator = "bookflight.arrivalradio.component")
	public BookFlightComp bookflightArrivalComp;
	
	
	@FindBy(locator = "bookflight.fightdetails.component")
	private QAFWebElement flightdetailsrowComp;
	
	@FindBy(locator = "bookflight.depaturetitle.lable")
	public QAFWebElement depaturetitle;
	
	@FindBy(locator = "bookflight.departFlightName.lable")
	public QAFWebElement departFlightName;
	
	@FindBy(locator = "bookflight.priceDepart.lable")
	public QAFWebElement priceDepart;
	
	@FindBy(locator = "bookflight.arrivaltitle.lable")
	public QAFWebElement arrivaltitle;
	
	@FindBy(locator = "bookflight.arrivalFlightName.lable")
	public QAFWebElement arrivalFlightName;
	
	@FindBy(locator = "bookflight.priceArrival.lable")
	public QAFWebElement priceArrival;
	
	@FindBy(locator = "bookflight.passengers.lable")
	public QAFWebElement passengers;
	
	@FindBy(locator = "bookflight.taxes.lable")
	public QAFWebElement taxes;
	
	@FindBy(locator = "bookflight.totalPrice.lable")
	public QAFWebElement totalPrice;
	
	@FindBy(locator = "bookflight.rowDetails.row")
	public  List<BookFlightComp> rowDetails;
    
	
	
	public List<BookFlightComp> getRowDetails() {
		return rowDetails;
	}

	public QAFWebElement getContinuebtn() {
		return continuebtn;
	}

	public QAFWebElement getNextContinueBtn() {
		return nextContinueBtn;
	}

	public BookFlightComp getBookflightcomponent() {
		return bookflightcomponent;
	}

	public BookFlightComp getBookflightArrivalComp() {
		return bookflightArrivalComp;
	}

	public QAFWebElement getFlightdetailsrowComp() {
		return flightdetailsrowComp;
	}

	public QAFWebElement getDepaturetitle() {
		return depaturetitle;
	}

	
	
	public QAFWebElement getContinueBtn() {
		return continuebtn;
	}

	public QAFWebElement getnextContinueBtn() {
		return nextContinueBtn;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
	driver.get("/");
		
	}
	
	public void setUp() {
		QEO20509_LoginPageTest login = new QEO20509_LoginPageTest();
		login.login();
		String actHomepagetitle = driver.getTitle();
		String expHomePageTitle ="Find a Flight: Mercury Tours";
		Validator.assertTrue(actHomepagetitle.contains(expHomePageTitle), "User not on loginPage", "User on LoginPAge");
		

	}
	
	
	public void roundtrip() {

		BookFlightComponentBean roundtrip = new BookFlightComponentBean();
		roundtrip.fillFromConfig("flight.book.component.data");
		roundtrip.fillUiElements();
		getContinueBtn().click();
		
		String actSelectFlightpagetitle = driver.getTitle();
		String expSelectflightPageTitle ="Select a Flight: Mercury Tours";
		Validator.assertTrue(actSelectFlightpagetitle.contains(expSelectflightPageTitle), "User not on Select a flight Page", "User on  Select a flight Page");
		
	}
	
	public void selectDepartFlight(String departFlightName, String arriveFlightName) {
		List<BookFlightComp> list = getRowDetails();
		for(BookFlightComp flight:list) {
			
			if(flight.getFlightName().getText().equals(departFlightName)) {
				flight.getDepartRdoBtnField().click();
			}
			else if(flight.getFlightName().getText().equals(arriveFlightName)) {
				flight.getArriveRdoBtnField().click();
				
			}
		}
		
	}
	


}
